/**
 * This example was written by Bruno Lowagie in answer to the following questions:
 * http://stackoverflow.com/questions/30106862/left-and-right-top-round-corner-for-rectangelroundrectangle
 */


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class BubbleSheet {

    public static final String DEST = "/home/prakash/Documents/bubbleSheet.pdf";
    public static final String IMG = "/home/prakash/Documents/a.png";

    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new BubbleSheet().createPdf(DEST);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();


        PdfPTable table1 = new PdfPTable(6);
        table1.setWidthPercentage(50.0f);
        PdfPTable table2 = new PdfPTable(6);
        table2.setWidthPercentage(50.0f);


        PdfPTable tableMain = new PdfPTable(2);


        //PdfPTable table = new PdfPTable(3);
        PdfPCell cell = getCell("a");
        table1.addCell(cell);
        cell = getCell("b");
        table1.addCell(cell);
        cell = getCell("c");
        table1.addCell(cell);
        cell = getCell("c");
        table1.addCell(cell);
        cell = getCell("c");
        table1.addCell(cell);
        cell = getCell("c");
        table1.addCell(cell);




        //table 2


        PdfPCell cell2 = getCell("a");
        table2.addCell(cell2);
        cell2 = getCell("b");
        table2.addCell(cell2);
        cell2 = getCell("c");
        table2.addCell(cell2);
        cell2 = getCell("c");
        table2.addCell(cell2);
        cell2 = getCell("c");
        table2.addCell(cell2);
        cell2 = getCell("c");
        table2.addCell(cell2);

        tableMain.addCell(table1);
        tableMain.addCell(table2);



        document.add(table1);
        document.add(table2);



        document.close();
    }

    public PdfPCell getCell(String content) throws IOException, BadElementException {

        Image img = Image.getInstance(IMG);
        img.scalePercent(25f);
        img.setAlignment(PdfPCell.ALIGN_CENTER);
        PdfPCell cell = new PdfPCell(new Phrase(new Chunk(img, 1f, 1f)));


        //cell.setCellEvent(new SpecialRoundedCell());
        cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);

        //cell.setBorder(PdfPCell.PTABLE);

        //cell.setPadding(5);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }
}