/**
 * Example written by Bruno Lowagie and Nishanthi Grashia in answer to the following question:
 * http://stackoverflow.com/questions/24359321/adding-row-to-a-table-in-pdf-using-itext
 */
//package sandbox.tables;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import javafx.scene.shape.Circle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//mport sandbox.WrapToTest;

//@WrapToTest
public class SimpleTable {
    public static final String DEST = "/home/prakash/Documents/simple_table.pdf";
    public static final String IMG = "/home/prakash/Documents/a.png";

    public static void main(String[] args) throws IOException,
            DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new SimpleTable().createPdf(DEST);
    }
    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        PdfWriter.getInstance(document, new FileOutputStream(dest));



        document.open();

        float[] columnWidth = {5,5,5,5,5,5,5};
        PdfPTable table = new PdfPTable(columnWidth);

        //PdfContentByte canvas = writer.getDirectContent();
//
//
//        PdfTemplate template = canvas.createTemplate(30, 30);
//
//        canvas.setLineWidth(2);


//        template .setLineWidth(1.5f);
//        template.circle(25f, 25f, 10);
//        template .stroke();
//
//        Image img = Image.getInstance(template);


        //PdfPCell cell = new PdfPCell();
        //cell.addElement(cell);



//        Phrase phrase = new Phrase(new Chunk(img, 1f, 1f));
//        PdfPCell cell = new PdfPCell(phrase);
//
//        table.addCell(cell);
//        System.out.println(table);
//
//
//
//
//
//        Phrase phrase1 = new Phrase(new Chunk(img, 1f, 1f));
//        PdfPCell cell1 = new PdfPCell(phrase1);
//
//        table.addCell(cell1);


        table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
        for(int aw = 0; aw < 7; aw++){

//            template .setLineWidth(1.5f);
//            template.circle(0+aw+10, 0+aw+10, 10);
//            template .stroke();
//
            Image img = Image.getInstance(IMG);
            img.scaleAbsolute(10,10);


            img.setScaleToFitLineWhenOverflow(true);

            //Phrase phrase = new Phrase(new Chunk(img, 1f, 1f));
            PdfPCell cell = new PdfPCell(img);
            cell.setBorder(PdfPCell.NO_BORDER);
            //cell.setFixedHeight(75);
//            cell.setPadding(5);
//            cell.setMinimumHeight(50);
//            cell.setPaddingTop(10);
            //cell.set
//            cell.setImage(img);

            //Image image = Image.getInstance(IMG);
            //PdfPCell cell = new PdfPCell(image);
//
//            table.addCell(cell);



            table.addCell(cell);


        }
        document.add(table);
        document.close();
    }

//    public static PdfPCell addCircle(int x,int y,int w,PdfWriter writer) throws DocumentException, IOException {
//
//        PdfContentByte canvas = writer.getDirectContent();
//        PdfPCell cell = new PdfPCell(canvas.circle(x,y,w), true);
//        return cell;
//    }

}