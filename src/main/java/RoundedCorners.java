/**
 * This example was written by Bruno Lowagie in answer to the following questions:
 * http://stackoverflow.com/questions/30106862/left-and-right-top-round-corner-for-rectangelroundrectangle
 */


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class RoundedCorners {

    public static final String DEST = "/home/prakash/Documents/rounded_corners.pdf";
    public static final String IMG1 = "/home/prakash/Documents/a.png";
    public static String  myString = "bhbhbhh";

    class SpecialRoundedCell implements PdfPCellEvent {
        public void cellLayout(PdfPCell cell, Rectangle position,
                               PdfContentByte[] canvases) {
            PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];


//            float llx = position.getLeft() + 3f;
//            float lly = position.getBottom() + 5f;
//            float urx = position.getRight() - 3f;
//            float ury = position.getTop() - 3f;
//            float r = 4;
//            float b = 0.4477f;
//            canvas.moveTo(llx, lly);
//            canvas.lineTo(urx, lly);
//            canvas.lineTo(urx, ury - r);
//            canvas.curveTo(urx, ury - r * b, urx - r * b, ury, urx - r, ury);
//            canvas.lineTo(llx + r, ury);
//            canvas.curveTo(llx + r * b, ury, llx, ury - r * b, llx, ury - r);
//            canvas.lineTo(llx, lly);
//            canvas.stroke();
//
//
//
//
//            canvas.saveState();
//            canvas.setColorStroke(GrayColor.BLACK);
//            canvas.setColorFill(GrayColor.WHITE);
//////            canvas.ellipse(
//////                    position.getLeft() - 3f, position.getBottom() - 3f,
//////                    position.getRight() + 3f, position.getTop() + 3f);
////            System.out.println("left"+cell.getLeft()+"  bottom"+cell.getBottom());
//            canvas.circle(llx,ury,10);
//            canvas.fillStroke();
//            canvas.restoreState();

//            canvas.setColorStroke(new GrayColor(0.8f));
//            canvas.roundRectangle(rect.getLeft() + 4, rect.getBottom(), rect.getWidth() - 8,
//                    rect.getHeight() - 4, 10);
//            canvas.stroke();
        }
    }

    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new RoundedCorners().createPdf(DEST);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));

//        Rotate event = new Rotate();
//        writer.setPageEvent(event);

//        PageOrientationsEventHandler eventHandler = new PageOrientationsEventHandler();
//        pdfDoc.addEventHandler(PdfDocumentEvent.START_PAGE, eventHandler);
//        Document doc = new Document(pdfDoc);


        document.open();


        PdfPTable table1 = new PdfPTable(7);

        PdfPTable table2 = new PdfPTable(7);

        PdfPTable table4 = new PdfPTable(1);
        PdfPTable table3 = new PdfPTable(1);



        for (int i =1;i<77;i++) {

            Boolean isNum;

            if (i == 1 ) {

                PdfPCell cell = getCell(i, i, true);
                table1.addCell(cell);
            }




            else if ((i % 7 == 1)&&i!=1){

                PdfPCell cell2 = getCell(i, i, true);
                table1.addCell(cell2);




            }

            else{

                PdfPCell cell3 = getCell(i, i,false );
                table1.addCell(cell3);

            }

        }



        for (int i =1;i<77;i++) {



            if (i == 1 ) {

                PdfPCell cell = getCell(i, i, true);
                table2.addCell(cell);
            }




            else if ((i % 7 == 1)&&i!=1){

                PdfPCell cell2 = getCell(i, i, true);
                table2.addCell(cell2);




            }

            else{

                PdfPCell cell3 = getCell(i, i,false );
                table2.addCell(cell3);

            }

        }


        PdfPTable tableMain = new PdfPTable(2);

        tableMain.setSpacingBefore(3f);

        PdfPCell cellt3 = new PdfPCell();
        cellt3.addElement(table1);

        cellt3.setBorderWidth(3f);
        cellt3.setPadding(1f);
        cellt3.setPaddingTop(1f);

        PdfPCell cellt4 = new PdfPCell();
        cellt4.addElement(table2);
        cellt4.setBorderWidth(3f);

        cellt4.setPadding(1f);
        cellt4.setPaddingTop(1f);


        PdfPCell cell = new PdfPCell();
        cell.addElement(table3);
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setPadding(10f);


        PdfPCell cell4 = new PdfPCell();
        cell4.addElement(table4);
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setPadding(10f);

        table3.addCell(cellt3);
        table3.setWidthPercentage(100);
        table4.addCell(cellt4);
        table4.setWidthPercentage(100);


        cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);




        tableMain.addCell(cell);
        tableMain.addCell(cell4);





        document.add(tableMain);

        BarcodeQRCode qrcode = new BarcodeQRCode(myString.trim(), 1, 1, null);
        Image qrcodeImage = qrcode.getImage();
        qrcodeImage.setAbsolutePosition(500,500);
        qrcodeImage.scalePercent(350);
        document.add(qrcodeImage);


        //event.setOrientation(PdfPage.LANDSCAPE);
        document.close();
    }

    public PdfPCell getCell(int imageNum,  int cellNum,Boolean isNum) throws IOException, BadElementException {

        int num;

        if (isNum) {

            if (cellNum==1){

                 num=0;

            }


             else {
                num = cellNum / 7;

            }

            num=num+1;

            String s = ""+num;


            PdfPCell num2 = new PdfPCell(new Phrase(s));
            num2.setBorder(PdfPCell.NO_BORDER);
            num2.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
            num2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);

            return num2;

        }

        else {
            imageNum=imageNum%7;
            String ii = "/home/prakash/Documents/o" + imageNum + ".png";
            System.out.println(ii);

            Image img = Image.getInstance(ii);
            img.scalePercent(25f);
            img.setAlignment(PdfPCell.ALIGN_CENTER);
            PdfPCell cell = new PdfPCell(new Phrase(new Chunk(img, 1f, 1f)));


            //cell.setCellEvent(new SpecialRoundedCell());
            cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
            cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            cell.setPadding(2f);

            //cell.setBorder(PdfPCell.PTABLE);

            //cell.setPadding(5);
            cell.setBorder(PdfPCell.NO_BORDER);
            return cell;
        }
    }



//    public class Rotate extends PdfPageEventHelper {
//
//        protected PdfNumber orientation = PdfPage.PORTRAIT;
//
//        public void setOrientation(PdfNumber orientation) {
//            this.orientation = orientation;
//        }
//
//        @Override
//        public void onStartPage(PdfWriter writer, Document document) {
//            writer.addPageDictEntry(PdfName.ROTATE, orientation);
//
//
//        }
//    }




}