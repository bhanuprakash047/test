/**
 * Example written by Bruno Lowagie and Nishanthi Grashia in answer to the following question:
 * http://stackoverflow.com/questions/24359321/adding-row-to-a-table-in-pdf-using-itext
 */
//package sandbox.tables;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import javafx.scene.shape.Circle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//mport sandbox.WrapToTest;

//@WrapToTest
public class Table2 {
    public static final String DEST = "/home/prakash/Documents/table2.pdf";
    public static final String IMG = "/home/prakash/Documents/a.png";

    public static void main(String[] args) throws IOException,
            DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new Table2().createPdf(DEST);
    }
    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        PdfWriter.getInstance(document, new FileOutputStream(dest));

        document.open();

        PdfPTable table = new PdfPTable(3);

        table.setTotalWidth(300);

        PdfContentByte canvas = writer.getDirectContent();
        PdfTemplate template = canvas .createTemplate(50, 50);




        table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
        for(int aw = 0; aw < 3; aw++){




            template .setLineWidth(2);
            template.circle(25f, 25f, 10);
            template .stroke();

            Image img = Image.getInstance(template);
            Phrase phrase = new Phrase(new Chunk(img, 1f, 1f));
            PdfPCell cell = new PdfPCell(phrase);

            table.addCell(cell);

//            Chunk chunk = new Chunk(img, 1f, 1f);
//            PdfPCell cell2 = new PdfPCell();
//            cell2.addElement(chunk);
//            table.addCell(cell2);



        }
        document.add(table);
        document.close();
    }

//    public static PdfPCell addCircle(int x,int y,int w,PdfWriter writer) throws DocumentException, IOException {
//
//        PdfContentByte canvas = writer.getDirectContent();
//        PdfPCell cell = new PdfPCell(canvas.circle(x,y,w), true);
//        return cell;
//    }

}