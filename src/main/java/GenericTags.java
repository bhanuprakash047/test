import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by prakash on 2/2/17.
 */
public class GenericTags extends PdfPageEventHelper {

    public static final String DEST = "/home/prakash/Documents/simple_table.pdf";

    @Override
    public void onGenericTag(PdfWriter writer, Document document, Rectangle rect, String text) {


        PdfContentByte canvas = writer.getDirectContent();
        //Rectangle rect = new Rectangle(36, 36, 559, 806);

        canvas.saveState();
        canvas.setColorFill(BaseColor.WHITE);
        canvas.ellipse(
                rect.getLeft() - 3f, rect.getBottom() - 5f,
                rect.getRight() + 3f, rect.getTop() + 3f);
        canvas.fill();

        canvas.restoreState();


    }


    public static void main(String a[]) throws FileNotFoundException, DocumentException {

        GenericTags gevent = new GenericTags();
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(DEST));
        PdfWriter.getInstance(document, new FileOutputStream(DEST));
        document.open();

       // writer.getPageEvent()= gevent;



    }

}