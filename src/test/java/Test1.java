import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**t1
 * Created by prakash on 25/1/17.
 */
public class Test1 {

    @Test

    public void  test1() throws MalformedURLException, InterruptedException {

        File appDir = new File("src");

        File app = new File(appDir, "test.apk");

        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,MobilePlatform.ANDROID);
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus 6");

        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100");
        cap.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        AndroidDriver driver =new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),cap );
        Thread.sleep(5000);
        List <WebElement> buttons = driver.findElements(By.className("android.widget.LinearLayout"));
        //login
        buttons.get(0).click();

        //username
        MobileElement loginButton = (MobileElement)driver.findElementByAndroidUIAutomator("new UiSelector().text(\"Enter Email\")");
        Assert.assertTrue(loginButton.isDisplayed(),"Enter email is not displayed");
        driver.findElementByAndroidUIAutomator("new UiSelector().text(\"Enter Email\")").sendKeys("hme-qatest43@example.com");
        //password
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/et_username\")").sendKeys("qatestpassword");

        //login button
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/btn_login_signup\")").click();

        //red fab button
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/fab_expand_menu_button\")").click();

        //Activity track
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/fab_add_activity\")").click();

        //search bar intitial
        driver.findElementByAndroidUIAutomator("new UiSelector().text(\"What activities did you do?\")").click();

        //activity search
        driver.findElementByAndroidUIAutomator("new UiSelector().text(\"Search physical activity\")").sendKeys("running");

        //click on running options
        driver.findElementByAndroidUIAutomator("new UiSelector().text(\"Running\")").click();

        MobileElement timeField = (MobileElement) driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/et_duration\")");
        Assert.assertTrue(timeField.isDisplayed(),"running time field is not displayed");

        //time in running
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/et_duration\")").sendKeys("10");

        //distance
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/et_distance\")").sendKeys("4");

        //fab button
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/fab\")").click();

        //overflow button
        driver.findElementByAndroidUIAutomator("resourceId(\"com.healthifyme.basic:id/iv_overflow_menu_card\")").click();

        //delete
        driver.findElementByAndroidUIAutomator("new UiSelector().text(\"Delete\")").click();



















//        //driver.findElementById("com.bt.bms:id/btnLogin").click();
//        driver.findElementById("com.bt.bms:id/btnSignUp").click();
//        int s=driver.findElements(By.className("android.widget.EditText")).size();
//        System.out.println(s);
//        List<WebElement> a=driver.findElements(By.className("android.widget.EditText"));
//        a.get(0).sendKeys("Appium");
//        a.get(1).sendKeys("Mobile");
//        a.get(2).sendKeys("appium@training");
//        a.get(3).sendKeys("password");
//        driver.findElementById("com.bt.bms:id/action_icon").click();

    }


}
