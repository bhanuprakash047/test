import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by prakash on 1/2/17.
 */
public class Circle {
    private void circle(float x, float y, PdfWriter writer) {
        PdfContentByte canvas = writer.getDirectContent();

        canvas.saveState();
        canvas.setLineWidth(3);
        canvas.setColorStroke(GrayColor.BLACK);
        canvas.setColorFill(GrayColor.WHITE);
        canvas.circle(x, y, 10);
        canvas.fillStroke();

        canvas.restoreState();
    }

    @Test
    public void testPossition() throws DocumentException, IOException, ParseException {
        String path ="/home/prakash/Documents/mypdf.pdf";

        JSONParser parser = new JSONParser();

        Object obj = parser.parse(new FileReader("/home/prakash/Documents/test.json"));
        JSONObject jsonObject = (JSONObject) obj;
        System.out.println(jsonObject);
        String myString = jsonObject.toString();

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();

        markPosition(100, 800, writer);
        markPosition(150, 800, writer);
        markPosition(200, 800, writer);
        markPosition(250, 800, writer);

        markPosition(100, 760, writer);
        markPosition(150, 760, writer);
        markPosition(200, 760, writer);
        markPosition(250, 760, writer);

        markPosition(100, 720, writer);
        markPosition(150, 720, writer);
        markPosition(200, 720, writer);
        markPosition(250, 720, writer);


        //document.add(new Paragraph("Total: 595 x 842 -- 72pt (1 inch)"));

        BarcodeQRCode qrcode = new BarcodeQRCode(myString.trim(), 1, 1, null);
        Image qrcodeImage = qrcode.getImage();
        qrcodeImage.setAbsolutePosition(10,500);
        qrcodeImage.scalePercent(200);
        document.add(qrcodeImage);

        document.close();
        writer.flush();
        writer.close();
    }

    private void markPosition(float x, float y, PdfWriter writer)
            throws DocumentException, IOException {
        //placeChunck("x: " + x + " y: " + y, x, y, writer);
        circle(x, y, writer);
    }

    private void placeChunck(String text, float x, float y, PdfWriter writer)
            throws DocumentException, IOException {
        PdfContentByte canvas = writer.getDirectContent();
        BaseFont font = BaseFont.createFont(BaseFont.HELVETICA,
                BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        canvas.saveState();
        canvas.beginText();
        canvas.moveText(x, y);
        canvas.setFontAndSize(font, 9);
        canvas.showText(text);
        canvas.endText();
        canvas.restoreState();
    }
}



